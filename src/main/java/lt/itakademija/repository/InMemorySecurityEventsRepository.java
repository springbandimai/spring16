package lt.itakademija.repository;

import lt.itakademija.model.EventRegistration;
import lt.itakademija.model.RegisteredEvent;
import lt.itakademija.model.RegisteredEventUpdate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * In-memory security events repository. Internally, it uses {@link SequenceNumberGenerator} and {@link DateProvider}.
 * <p>
 * Created by mariusg on 2016.12.19.
 */
@Component
public final class InMemorySecurityEventsRepository implements SecurityEventsRepository {

    private final SequenceNumberGenerator sequenceGenerator;

    private final DateProvider dateProvider;

    private List<RegisteredEvent> eventList;

    public InMemorySecurityEventsRepository(SequenceNumberGenerator sequenceGenerator, DateProvider dateProvider) {
        this.sequenceGenerator = sequenceGenerator;
        this.dateProvider = dateProvider;
        this.eventList = new ArrayList<>();
    }

    /*
     *  Notes for implementation:
     *  
     *  This method must use SequenceNumberGenerator#getNext() for generating ID;
     *  This method must use DateProvider#getCurrentDate() for getting dates;
     */
    @Override
    public RegisteredEvent create(EventRegistration eventRegistration) {
        //throw new UnsupportedOperationException("not implemented");
        RegisteredEvent eventReg = new RegisteredEvent(
                this.sequenceGenerator.getNext(),
                this.dateProvider.getCurrentDate(),
                eventRegistration.getSeverityLevel(),
                eventRegistration.getLocation(),
                eventRegistration.getDescription()
        );
        eventList.add(eventReg);
        return eventReg;
    }

    @Override
    public List<RegisteredEvent> getEvents() {
        //throw new UnsupportedOperationException("not implemented");
        return eventList;
    }

    @Override
    public RegisteredEvent delete(Long id) {

        RegisteredEvent deletedEvent = null;
        for (RegisteredEvent r : eventList
                ) {
            deletedEvent = r;
            if (r.getId() == id) {
                eventList.remove(r);
            }
            return deletedEvent;
        }
        return null;
    }

    @Override
    public RegisteredEvent update(Long id, RegisteredEventUpdate registeredEventUpdate) {

        RegisteredEvent oldEvent = null;
        RegisteredEvent newEvent = null;

        for (RegisteredEvent r : eventList
                ) {
            oldEvent = r;
            if (r.getId() == id) {
                newEvent = new RegisteredEvent(
                        r.getId(),
                        r.getRegistrationDate(),
                        registeredEventUpdate.getSeverityLevel(),
                        r.getLocation(),
                        r.getDescription()
                );
            }
        }
        eventList.remove(oldEvent);
        eventList.add(newEvent);
        return newEvent;
    }
}

