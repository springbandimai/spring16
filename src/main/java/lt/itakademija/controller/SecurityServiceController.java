package lt.itakademija.controller;

import lt.itakademija.model.EventRegistration;
import lt.itakademija.model.RegisteredEvent;
import lt.itakademija.model.RegisteredEventUpdate;
import lt.itakademija.repository.SecurityEventsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/webapi/events")
public class SecurityServiceController {

    @Autowired
    private final SecurityEventsRepository repository;

    public SecurityServiceController(final SecurityEventsRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<RegisteredEvent> getRegisteredEvents() {
        return repository.getEvents();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public RegisteredEvent createEvent(@RequestBody @Valid EventRegistration registrationData) {
        return repository.create(registrationData);
    }

    @RequestMapping(path = "/{eventId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public RegisteredEvent deleteEvent(@PathVariable Long id) {
        return repository.delete(id);
    }

    @RequestMapping(path = "/{eventId}", method = RequestMethod.PUT)
    public RegisteredEvent updateEvent(@PathVariable Long id, @RequestBody @Valid RegisteredEventUpdate updateData) {
        return repository.update(id, updateData);
    }

}
